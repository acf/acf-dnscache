local mymodule = {}

mymodule.default_action = "status"

function mymodule.status(self)
	return self.model.getstatus()
end

function mymodule.startstop(self)
	return self.handle_form(self, self.model.get_startstop, self.model.startstop_service, self.clientdata)
end

function mymodule.config(self)
	return self.handle_form(self, self.model.getconfig, self.model.setconfig, self.clientdata, "Save", "Edit Config", "Configuration Set")
end

function mymodule.expert(self)
	return self.handle_form(self, self.model.getconfigfile, self.model.setconfigfile, self.clientdata, "Save", "Edit Config File", "Configuration File Set")
end

function mymodule.editips(self)
	return self.handle_form(self, self.model.getIPs, self.model.setIPs, self.clientdata, "Save", "Edit IP List", "IP List Set")
end

function mymodule.listdomains(self)
	return self.model.getDomains()
end

function mymodule.createdomain(self)
	return self.handle_form(self, self.model.getNewDomain, self.model.setNewDomain, self.clientdata, "Create", "Create New Domain", "New Domain Created")
end

function mymodule.editdomain(self)
	return self.handle_form(self, self.model.getDomain, self.model.setDomain, self.clientdata, "Save", "Edit Domain Entry", "Domain Saved")
end

function mymodule.deletedomain(self)
	return self.handle_form(self, self.model.getDeleteDomain, self.model.deleteDomain, self.clientdata, "Delete", "Delete Domain Entry", "Domain Deleted")
end

return mymodule
