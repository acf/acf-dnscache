<% local data, viewlibrary, page_info, session = ...
htmlviewfunctions = require("htmlviewfunctions")
html = require("acf.html")
%>

<script type="text/javascript">
	if (typeof jQuery == 'undefined') {
		document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery-latest.js"><\/script>');
	}
</script>

<script type="text/javascript">
	if (typeof $.tablesorter == 'undefined') {
		document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery.tablesorter.js"><\/script>');
	}
</script>

<script type="text/javascript">
	$(document).ready(function() {
		$("#list").tablesorter({widgets: ['zebra']});
	});
</script>

<% htmlviewfunctions.displaycommandresults({"editdomain", "deletedomain"}, session) %>
<% htmlviewfunctions.displaycommandresults({"createdomain"}, session, true) %>

<%
local header_level = htmlviewfunctions.displaysectionstart(cfe({label="Configuration"}), page_info)
local header_level2 = htmlviewfunctions.displaysectionstart(cfe({label="Edit/View DNS server entries"}), page_info, htmlviewfunctions.incrementheader(header_level))
%>
<table id="list" class="tablesorter"><thead>
	<tr>
		<th>Action</th>
		<th>Domain</th>
	</tr>
</thead><tbody>
<% local domain = cfe({ type="hidden", value="" }) %>
<% local redir = cfe({ type="hidden", value=page_info.orig_action }) %>
<% for i,d in ipairs(data.value) do %>
	<tr>
		<td>
			<%
			domain.value = d
			htmlviewfunctions.displayitem(cfe({type="link", value={domain=domain, redir=redir}, label="", option="Edit", action="editdomain"}), page_info, -1)
			if d ~= "@" then
				htmlviewfunctions.displayitem(cfe({type="form", value={domain=domain}, label="", option="Delete", action="deletedomain"}), page_info, -1)
			end
			%>
		</td>
		<td><%= html.html_escape(d) %></td>
	</tr>
<% end %>
</tbody></table>
<%
htmlviewfunctions.displaysectionend(header_level2)
htmlviewfunctions.displaysectionend(header_level)
%>

<% if viewlibrary and viewlibrary.dispatch_component then
	viewlibrary.dispatch_component("createdomain")
end %>
